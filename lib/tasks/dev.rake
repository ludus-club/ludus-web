namespace :dev do
  PASSWORD_DEFAULT = 123456

  desc "Configura o ambiente de desenvolvimento"
  task setup: :environment do
    if Rails.env.development?

      show_spinner("Apagando DB...") { %x(rails db:drop) }
      show_spinner("Criando DB...") { %x(rails db:create) }
      show_spinner("Migrando DB...") { %x(rails db:migrate) }
      show_spinner("Populando DB...") { %x(rails db:seed) }
      show_spinner("Cadastrando o administrador padrão...") { %x(rails dev:add_default_admin) }
      show_spinner("Cadastrando administradores extras...") { %x(rails dev:add_extras_admins) }
      show_spinner("Cadastrando o usuário padrão...") { %x(rails dev:add_default_user) }

    else
      puts "Voce nao esta em ambiente de desenvolvimento!"
    end
  end


  desc "Adiciona o administrador padrão"
  task add_default_admin: :environment do
    Admin.create!(
      email: 'admin@admin.com',
      password: PASSWORD_DEFAULT,
      password_confirmation: PASSWORD_DEFAULT
    )
  end

  desc "Adiciona administradores extras"
  task add_extras_admins: :environment do
    10.times do |i|
      Admin.create!(
        email: Faker::Internet.email,
        password: PASSWORD_DEFAULT,
        password_confirmation: PASSWORD_DEFAULT
      )
    end
  end

  desc "Adiciona o usuario padrão"
  task add_default_user: :environment do
    User.create!(
      email: 'user@user.com',
      password: PASSWORD_DEFAULT,
      password_confirmation: PASSWORD_DEFAULT
    )
  end

  private

  def show_spinner(msg_init, msg_end="Concluído!")
    spinner = TTY::Spinner.new("[:spinner] #{msg_init}")
    spinner.auto_spin

    yield
    
    spinner.success("(#{msg_end})")
  end
end
